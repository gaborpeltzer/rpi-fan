import sys
sys.path.append('/storage/.kodi/addons/virtual.rpi-tools/lib')
import os
from time import sleep
import signal
import RPi.GPIO as GPIO
import re

# crontab * * * * * python /storage/fan.py > /dev/null 2>&1

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
control_pin = 17
GPIO.setup(control_pin, GPIO.OUT)

def measure_temp():
	raw = os.popen('vcgencmd measure_temp').readline()
	m = re.match("temp=(\d+\.?\d*)'C", raw)
	if not m:
		raise ValueError("Unexpected temperature string:" + raw)
	return float(m.group(1))

temp = measure_temp()
print 'Temperature from vcgencmd: {}'.format(temp)

if temp < 45:
	print 'Turn off'
	GPIO.output(control_pin, False)
else:
	print 'Turn on'
	GPIO.output(control_pin, True)
